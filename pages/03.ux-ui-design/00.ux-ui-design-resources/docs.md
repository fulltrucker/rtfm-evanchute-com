---
title: UX/UI Design Resources
taxonomy:
  category:
    - docs
routes:
  canonical: /ux-ui-design/ux-ui-design-resources
---
This will eventually need to be broken down to separate pages by category, or something. But for now it can be a big-ass list.

## Education and Learning

* [Digital Society School](https://digitalsocietyschool.org/)
* [Design Method Toolkit](https://toolkits.dss.cloud/design/)
* [The Encyclopedia of Human-Computer Interaction, 2nd Ed.](https://www.interaction-design.org/literature/book/the-encyclopedia-of-human-computer-interaction-2nd-ed/)

## UX/UI Design Tools

### Sketch

Soup to nuts, industry standard UX/UI design tool. Does all the shit, but it has some weird nuances.

- https://www.sketch.com/
- https://www.sketch.com/extensions/plugins/
- https://www.sketchappsources.com/
- https://www.google.com/search?q=sketch+plugins
- https://sketchapphub.com/
- https://sketchtalk.io/
- https://designcode.io/ios11-ui-kit

#### Make Sketch Great Again

* Signup for InVision https://www.invisionapp.com/
* Download Craft by InVision https://www.invisionapp.com/craft
* Hook all that shit up

### Adobe XD

The biggest player in the design game's entry into the XD space, more intuitive than Sketch but scuttlebutt is that it's lacking in some features compared to other tools. https://www.adobe.com/products/xd.html

### Figma

Collaborative. New player on the block. Seems to be a lot of buzz around this tool. https://www.figma.com/

### UXPin

I don't no nothing about this tool really, except that they sent me WAY to many emails. https://www.uxpin.com/

### Draw.io

Can be used for user workflows and diagramming. I think this is similar to Gliffy, which I've used for sitemaps, information diagrams, server infrastructure modeling, and shit like that there. https://www.draw.io/

### Balsamiq

Great for quick and dirty lo-fi wireframing, with simple prototyping functionality. Not good (or really even possible to be used) for hi-fidelity UI design. Has a "sketch" display mode that make all elements look hand-drawn, so it's nice for early client presentations where you don't want the client to get hung up on design details. Use Balsamiq (Comic) Sans font for this. https://balsamiq.com/

### Adobe Creative Suite

Illustrator and Photoshop, mainly. No prototyping functionality, at least none that I know of. It was all we had 10 years ago, and it was enough. Get off my lawn!

## UI Starter Kits

### Bootstrap

* Bootstrap UI: https://getbootstrap.com/docs/3.4/components/
* Bootstrap UI Sketch: https://www.sketchappsources.com/free-source/1768-bootstrap-v4-ui-kit-sketch-freebie-resource.html
* http://bootflat.github.io/index.html
* http://bootflat.github.io/color-picker.html
* http://bootstrapuikit.com/

### Material Design

* https://material.io
* https://material.io/design/
* https://material.io/resources/
* https://materialdesignkit.com/android-gui/
* https://dribbble.com/tags/material_design
* [Material Sketch Library](https://www.sketchappsources.com/free-source/874-material-design-google-sketch-freebie-resource.html)

### Semantic UI

* https://semantic-ui.com/
* List of UI components in this framework: https://semantic-ui.com/kitchen-sink.html

## List of UI Elements

- https://www.usability.gov/how-to-and-tools/methods/user-interface-elements.html
- https://careerfoundry.com/en/blog/ui-design/ui-element-glossary/
  - Bento Menu!!!
- https://developers.google.com/style/ui-elements

## Fonts and Glyphs

### FontAwesome

* https://fontawesome.com/icons
* https://fontawesome.com/cheatsheet
* Copy the glyph or the unicode, and paste it into a textbox that is set to the FontAwesome font
* You can also just type the name of the icon into the textbox e.g. `trash`

### Glyphicon

* Glyphicon is a Bootstrap thing, there's probably a Sketch plugin to insert glyphicons

### Google Fonts

* https://fonts.google.com/ 

## User Testing

* UserZoom.com
* UserTesting.com
* OptimalWorkshop.com

## Miscellaneous

* [Toggl](https://toggl.com/) for time tracking
  * https://toggl.com/app/timer Timer App UI, Create an account and use for timing projects. 
  * Count the UI Components for a fun game!
* Denver Users: http://denveruxers.com/
* Design Sprints: https://www.gv.com/sprint/

