---
title: UX/UI Article List
taxonomy:
  category:
    - docs
routes:
  canonical: /ux-ui-design/light-reading
---
## Jargon, Vernacular, and Concepts

* [32 User Interface Elements For UI Designers: Your Ultimate Glossary](https://careerfoundry.com/en/blog/ui-design/ui-element-glossary/)
* [A list of design concepts every UX/UI designer should learn](https://uxdesign.cc/a-list-of-design-concepts-every-ux-ui-designer-should-learn-7e2d8412b391)
* [The book that pushed my design skills to the next level in 2017](https://blog.prototypr.io/the-one-book-that-ive-read-in-2017-that-pushed-my-design-skills-further-1876ed670b6d)

## Visual Design

* https://www.interaction-design.org/literature/topics/graphic-design
* [4 things I wish I knew before making the shift from Graphic Design to UX Design](https://uxdesign.cc/3-things-i-wish-i-knew-before-i-made-the-shift-from-graphic-design-to-ux-design-655af468c923)

## Coding and Development

* [Should designers learn front-end development? 11 UX-perts weigh in](https://www.justinmind.com/blog/should-designers-learn-front-end-development-10-ux-perts-weigh-in/)

## General Stuff

* [The Encyclopedia of Human-Computer Interaction, 2nd Ed.](https://www.interaction-design.org/literature/book/the-encyclopedia-of-human-computer-interaction-2nd-ed/)
* [UX has bad UX](https://uxdesign.cc/ux-has-pretty-bad-ux-fd2702e5c22a)

### Best Practices

* [Best Practices for Search](https://www.uxbooth.com/articles/best-practices-for-search/)
* [Responsive Design vs. Adaptive Design: What’s the Best Choice for Designers?](https://www.uxpin.com/studio/blog/responsive-vs-adaptive-design-whats-best-choice-designers/)

